#------------------------------------------------------------------------------
#          Harvard University Atmospheric Chemistry Modeling Group            !
#------------------------------------------------------------------------------
#BOP
#
# !MODULE: Makefile
#
# !DESCRIPTION: Main makefile for the NCL4GC package.  Builds shared object
#  libraries as well as the NCL4GC documentation.
#\\
#\\
# !REMARKS:
# To build libraries and/or documentation, call "make" with this syntax:
#                                                                             .
#   make TARGET [ OPTIONAL-FLAGS ]
#                                                                             .
# To display a complete list of options, type "make help".
#                                                                             .
# You must have the LaTeX utilities (latex, dvips, dvipdf) installed
# on your system in order to build the documentation.
#
# !REVISION HISTORY: 
#  06 Dec 2013 - R. Yantosca - Initial version
#EOP
#------------------------------------------------------------------------------
#BOC

# Unix shell
SHELL :=/bin/bash

# Directories
DOC   := ./doc
F77   := ./fortran
HELP  := ./help

#==============================================================================
# Makefile targets: type "make help" for a complete list!
#==============================================================================

.PHONY: all lib doc libclean docclean realclean help

#-----------------------------------
# Build libraries & documentation 
#-----------------------------------

all: lib doc

lib:
	@$(MAKE) -C $(F77) all

doc:
	@$(MAKE) -C $(DOC) all

#-----------------------------------
# Remove libraries & documentation
#-----------------------------------

realclean: libclean docclean

libclean:
	@$(MAKE) -C $(F77) clean

docclean:
	@$(MAKE) -C $(DOC) clean

#-----------------------------------
# Display help screen 
#-----------------------------------

help:
	$(MAKE) -C $(HELP) all
#EOC
