;------------------------------------------------------------------------------
;    NCL4GC: NCL Tools for use with the GEOS-Chem Chemical Transport Model    !
;------------------------------------------------------------------------------
;BOP
;
; !ROUTINE: get_float_from_bpch
;
; !DESCRIPTION: Reads an array of float (aka REAL*4) from a binary punch file,
;  which is the traditional GEOS-Chem output file format.
;\\
;\\
; !USES:
;
 ; NCL code
 load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"  
;
; !INTERFACE:
;
 undef( "get_float_from_bpch" )
 function get_float_from_bpch( inFile     : string, 
                               recNum     : integer, 
                               dimensions : integer  )
;
; !INPUT PARAMETERS:
;  inFile     : Name of the bpch file containing the desired string fields.
;  recNum     : Record number in the binary punch file (i.e. the order in 
;                which each data field is stored, starting from 0).  The value
;                of recNum will be incremented by 1 each time this routine
;                is called.  This allows you to initialize a record counter
;                in the calling routine, which can be passed to each 
;                successive call of get_string_from_bpch.
;  dimensions : Number of bytes to read in each dimension.  NOTE: this has
;                to be in NCL order, not FORTRAN order.  If -1, it will 
;                read all bytes in the record.
;
; !RETURN VALUE:
;  floatArray : Array of float data (aka REAL*4) from the record in the
;                binary punch file.

; !LOCAL VARIABLES:
;
 local floatArray
;
; !REMARKS:
;  (1) The format of the binary punch file is discussed in Ch. 6 of the GAMAP
;       User's Manual: http://acmg.seas.harvard.edu/gamap/doc/Chapter_6.html.
;  (2) This routine also can read data from F77-unformatted binary sequential
;       files other than the specific binary punch file format.
;
; !REVISION HISTORY:
;  05 Dec 2013 - R. Yantosca - Initial version
;EOP
;------------------------------------------------------------------------------
;BOC
begin

  ; Read an array of characters from the bpch file
  floatArray  = fbinrecread( inFile, recNum, dimensions, "float" )

  ; Increment record number and return string to calling routine
  recNum = recNum + 1
  return( floatArray )

end
;EOC