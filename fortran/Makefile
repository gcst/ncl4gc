#------------------------------------------------------------------------------
#          Harvard University Atmospheric Chemistry Modeling Group            !
#------------------------------------------------------------------------------
#BOP
#
# !MODULE: Makefile (in NGL4GC/fortran directory)
#
# !DESCRIPTION: Makefile for building shared object libraries (via the NCL
#  "WRAPIT" command) that can be linked to NCL4GC routines.
#\\
#\\
# !REMARKS:
# To build libraries and/or documentation, call "make" with this syntax:
#                                                                             .
#   make TARGET [ OPTIONAL-FLAGS ]
#                                                                             .
# To display a complete list of options, type "make help".
#                                                                             .
# You must have the LaTeX utilities (latex, dvips, dvipdf) installed
# on your system in order to build the documentation.
#
# !REVISION HISTORY: 
#  06 Dec 2013 - R. Yantosca - Initial version
#EOP
#------------------------------------------------------------------------------
#BOC

#==============================================================================
# Initialization
#==============================================================================

# Use bash shell
SHELL      :=/bin/bash

# Base command 
WRAPIT_CMD :=WRAPIT

# Default compiler is IFORT
ifndef COMPILER
COMPILER   :=ifort
endif

# Set switch for IFORT compiler
REGEXP     :=(^[Ii][Ff][Oo][Rr][Tt])
ifeq ($(shell [[ "$(COMPILER)" =~ $(REGEXP) ]] && echo true),true)
WRAPIT_CMD += -in
endif

# Set switch for PGI compiler
REGEXP     :=(^[Pp][Gg][Ii])
ifeq ($(shell [[ "$(COMPILER)" =~ $(REGEXP) ]] && echo true),true)
WRAPIT_CMD += -pg
endif

# Set switch for LAHEY compiler
REGEXP     :=(^[Ll][Aa][Hh][Ee][Yy])
ifeq ($(shell [[ "$(COMPILER)" =~ $(REGEXP) ]] && echo true),true)
WRAPIT_CMD += -lf
endif

# Set switch for G95 compiler
REGEXP     :=(^[Gg]95)
ifeq ($(shell [[ "$(COMPILER)" =~ $(REGEXP) ]] && echo true),true)
WRAPIT_CMD += -g95
endif

# Set switch for G95 compiler
REGEXP     :=(^[Gg]77)
ifeq ($(shell [[ "$(COMPILER)" =~ $(REGEXP) ]] && echo true),true)
WRAPIT_CMD += -g77
endif

# Set switch for GFORTRAN compiler
REGEXP     :=(^[Gg][Ff][Oo][Rr][Tt][Rr][Aa][Nn])
ifeq ($(shell [[ "$(COMPILER)" =~ $(REGEXP) ]] && echo true),true)
WRAPIT_CMD += -gf
endif

# Set debug switch
REGEXP     := (^[Yy]|^[Yy][Ee][Ss])
ifeq ($(shell [[ "$(DEBUG)" =~ $(REGEXP) ]] && echo true),true)
WRAPIT_CMD += -d
endif

#==============================================================================
# Targets
#==============================================================================

.PHONY: all libclean

#-----------------------------------
# Build libraries 
#-----------------------------------

all: lib

lib:
	$(WRAPIT_CMD) binary.f

libclean: clean


#-----------------------------------
# Remove libraries & documentation
#-----------------------------------
clean:
	rm -f *.o *.so *.mod

#-----------------------------------
# Display WRAPIT command
#-----------------------------------

print:
	@echo "WRAPIT_CMD: $(WRAPIT_CMD)"	
#EOC
