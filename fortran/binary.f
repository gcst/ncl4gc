C------------------------------------------------------------------------------
C    NCL4GC: NCL Tools for use with the GEOS-Chem Chemical Transport Model    !
C------------------------------------------------------------------------------
CBOP
C
C !MODULE: binary 
C
C !DESCRIPTION: Contains routines for parsing a string of bytes read by NCL
C  from a GEOS-Chem bpch file or binary met field data file into Fortran 
C  variables (INTEGER, REAL*4, and REAL*8 types).
C\\
C\\
C !REMARKS:
C  (1) This file must be named bpch.f, or else the WRAPIT script 
C       (used by NCL to create a shared library) won't work properly.
C  (2) The code must be placed into strict F77 format (i.e none of the
C        new language features of F90).  We have added the ProTeX headers
C        for clarity, but have replaced all ! with C's in the first column.
C  (3) To build the shared library for this module, use:
C       WRAPIT -in bpch.f    # for Intel ifort compiler
C       WRAPIT -pg bpch.f    # for PGI compiler
C 
C !REVISION HISTORY: 
C  04 Dec 2013 - R. Yantosca - Initial version
C  06 Dec 2013 - R. Yantosca - Remove ! from ProTeX headers, ProTeX now uses
C                              -7 option for strict F77 format code
C  12 Mar 2014 - R. Yantosca - Renamed routine HDR1 to BPCH_HDR1
C  12 Mar 2014 - R. Yantosca - Renamed routine HDR2 to BPCH_HDR2
C  12 Mar 2014 - R. Yantosca - Added routine MET_HDR
C  12 Mar 2014 - R. Yantosca - Renamed to "binary.f"
CEOP
C------------------------------------------------------------------------------
C          Harvard University Atmospheric Chemistry Modeling Group            !
C------------------------------------------------------------------------------
CBOP
C
C !IROUTINE: bpch_hdr1
C
C !DESCRIPTION: Reads 36 bytes from the first line of a bpch data block 
C  header and converts and returns the MODELNAME, LON, LAT, HALFPOLAR, and
C  CENTER180 fields.
C\\
C\\
C !INTERFACE:
C
C NCLFORTSTART
      SUBROUTINE BPCH_HDR1( BYTES, MODELNAME, LON, 
     &                      LAT,   HALFPOLAR, CENTER180 )
C
C !INPUT PARAMETERS: 
C
      CHARACTER*36  BYTES       ! 36 bytes from 1st line of bpch data header
C
C !OUTPUT PARAMETERS:
C
      CHARACTER*20  MODELNAME   ! Model name
      REAL*4        LON         ! Lon resolution [deg]
      REAL*4        LAT         ! Lat resolution [deg]
      INTEGER       HALFPOLAR   ! Half-polar boxes?
      INTEGER       CENTER180   ! Grid centered on 180?
C NCLEND
C
C !REMARKS:
C  (1) We have to use the CHARACTER type to pass unsigned bytes from NCL.
C  (2) The input string of bytes is returned by NCL's fbinrecread function.
C  (3) Uses byte routines from John Burkardt (included here)
C 
C !REVISION HISTORY: 
C  04 Dec 2013 - R. Yantosca - Initial version
CEOP
C------------------------------------------------------------------------------
CBOC
C
C !LOCAL VARIABLES:
C
      INTEGER     I
      INTEGER     X
      CHARACTER*1 C(36)

C     !----------------------------------------------------------------
C     ! Parse first bpch data block line
C     ! (Bytes are returned from NCL's fbinrecread function)
C     !----------------------------------------------------------------

C     ! Split characters into an array for use 
C     ! w/ the byte conversion routines below
      DO I = 1, 36
         C(I) = BYTES(I:I)
      ENDDO

C     ! Get MODELNAME
      DO I = 1, 20
         MODELNAME(I:I) = C(I)
      ENDDO

C     ! Get LON (first make a 4-byte word, then convert to REAL*4)
      CALL B4_TO_UI4    ( C(21:24), X         )
      CALL B4_IEEE_TO_R4( X,        LON       )

C     ! Get LAT value (first make a 4-byte word, then convert to REAL*4)
      CALL B4_TO_UI4    ( C(25:28), X         )
      CALL B4_IEEE_TO_R4( X,        LAT       )

C     ! Get HALFPOLAR
      CALL B4_TO_UI4    ( C(29:32), HALFPOLAR )

C     ! Get CENTER180
      CALL B4_TO_UI4    ( C(33:36), CENTER180 )

      RETURN
      END SUBROUTINE BPCH_HDR1
CEOC
C------------------------------------------------------------------------------
C          Harvard University Atmospheric Chemistry Modeling Group            !
C------------------------------------------------------------------------------
CBOP
C
C !IROUTINE: bpch_hdr2
C
C !DESCRIPTION: Reads 168 bytes from the second line of a bpch data block 
C  header and converts and returns the CATEGORY, TRACER, UNIT, TAU0, TAU1,
C  RESERVED, NI, NJ, NL, I0, J0, L0, and SKIP fields.
C\\
C\\
C !INTERFACE:
C
C NCLFORTSTART
      SUBROUTINE BPCH_HDR2( BYTES, CATEGORY, TRACER,   UNIT,
     &                      TAU0,  TAU1,     RESERVED, NI,
     &                      NJ,    NL,       I0,       J0,
     &                      L0,    SKIP                      )
C
C !INPUT PARAMETERS: 
C
      CHARACTER*168  BYTES       ! 168 bytes from 2nd line of bpch data header
C
C !OUTPUT PARAMETERS:
C
      CHARACTER*40   CATEGORY    ! Diagnostic category
      INTEGER        TRACER      ! Tracer number
      CHARACTER*40   UNIT        ! Unit string
      REAL*8         TAU0        ! Starting time
      REAL*8         TAU1        ! Ending time 
      CHARACTER*40   RESERVED    ! Reserved string
      INTEGER        NI, NJ, NL  ! Dimensions of data
      INTEGER        I0, J0, L0  ! Starting loc of data
      INTEGER        SKIP        ! # of bytes to skip
C NCLEND
C
C !REMARKS:
C  (1) The input string of bytes is returned by NCL's fbinrecread function.
C  (2) We have to use the CHARACTER type to pass unsigned bytes from NCL.
C  (3) Uses byte routines from John Burkardt (included here)
C 
C !REVISION HISTORY: 
C  04 Dec 2013 - R. Yantosca - Initial version
CEOP
C------------------------------------------------------------------------------
CBOC
C
C !LOCAL VARIABLES:
C
      INTEGER     I
      INTEGER*8   X
      CHARACTER*1 C(168)

C     ! Split characters into an array for use 
C     ! w/ the byte conversion routines below
      DO I = 1, 168
         C(I) = BYTES(I:I)
      ENDDO

C     ! Get CATEGORY, UNIT, RESERVED
      DO I = 1, 40
         CATEGORY(I:I) = C(    I)
         UNIT    (I:I) = C( 44+I)
         RESERVED(I:I) = C(100+I)
      ENDDO

C     ! Get TRACER value
      CALL B4_TO_UI4( C( 41: 44), TRACER )

C     ! Get TAU0 (convert to 8-byte word, then to REAL*8)
      CALL B8_TO_UI8( C(85:92), X )
      CALL B8_IEEE_TO_R8( X, TAU0 )

C     ! Get TAU1 (convert to 8-byte word, then to REAL*8)
      CALL B8_TO_UI8( C(93:100), X )
      CALL B8_IEEE_TO_R8( X, TAU1 )

C     ! Get NI, NJ, NL values
      CALL B4_TO_UI4( C(141:144), NI     )
      CALL B4_TO_UI4( C(145:148), NJ     )
      CALL B4_TO_UI4( C(149:152), NL     )

C     ! Get I0, J0, L0 values
      CALL B4_TO_UI4( C(153:156), I0     )
      CALL B4_TO_UI4( C(157:160), J0     )
      CALL B4_TO_UI4( C(161:164), L0     )

C     ! Get SKIP value
      CALL B4_TO_UI4( C(165:168), SKIP   )

      RETURN
      END SUBROUTINE BPCH_HDR2
CEOC
C------------------------------------------------------------------------------
C          Harvard University Atmospheric Chemistry Modeling Group            !
C------------------------------------------------------------------------------
CBOP
C
C !IROUTINE: met_hdr
C
C !DESCRIPTION: Reads 8 bytes from a binary met field data block header
C  and returns the YYYYMMDD date and HHMMSS time.
C\\
C\\
C !INTERFACE:
C
C NCLFORTSTART
      SUBROUTINE MET_HDR( BYTES, YYYYMMDD, HHMMSS )
C
C !INPUT PARAMETERS: 
C
      CHARACTER*8 BYTES          ! bytes from binary met data file
      INTEGER     NI             ! Longitude dimension of ARRAY
      INTEGER     NJ             ! Latitude dimension of array
C
C !OUTPUT PARAMETERS:
C
      INTEGER     YYYYMMDD       ! Date 
      INTEGER     HHMMSS         ! Time
C NCLEND
C
C !REMARKS:
C  (1) We have to use the CHARACTER type to pass unsigned bytes from NCL.
C  (2) The input string of bytes is returned by NCL's fbinrecread function.
C  (3) Uses byte routines from John Burkardt (included here)
C 
C !REVISION HISTORY: 
C  04 Dec 2013 - R. Yantosca - Initial version
CEOP
C------------------------------------------------------------------------------
CBOC
C
C !LOCAL VARIABLES:
C
      INTEGER     I
      CHARACTER*1 C(8)

C     !----------------------------------------------------------------
C     ! Parse first bpch data block line
C     ! (Bytes are returned from NCL's fbinrecread function)
C     !----------------------------------------------------------------

C     ! Split characters into an array for use 
C     ! w/ the byte conversion routines below
      DO I = 1, 8
         C(I) = BYTES(I:I)
      ENDDO

C     ! Get YYYYMMDD
      CALL B4_TO_UI4( C(1:4), YYYYMMDD )

C     ! Get HHMMSS
      CALL B4_TO_UI4( C(5:8), HHMMSS   )

      RETURN
      END SUBROUTINE MET_HDR 
CEOC
C------------------------------------------------------------------------------
C          Harvard University Atmospheric Chemistry Modeling Group            !
C------------------------------------------------------------------------------
CBOP
C
C !IROUTINE: met_2d
C
C !DESCRIPTION: Reads a 2D array from a binary met field data file
C  and converts to a REAL*4 array of size (NI,NJ).
C\\
C\\
C !INTERFACE:
C
C NCLFORTSTART
      SUBROUTINE MET_2D( BYTES, NI, NJ, ARRAY )
C
C !INPUT PARAMETERS: 
C
      CHARACTER*13248 BYTES          ! Bytes from binary met data file
      INTEGER         NI             ! Longitude dimension of ARRAY
      INTEGER         NJ             ! Latitude dimension of ARRAY
C
C !OUTPUT PARAMETERS:
C
      REAL*4          ARRAY(NI,NJ)   ! Array of REAL*4 data
C NCLEND
C
C !REMARKS:
C  (1) We have to use the CHARACTER type to pass unsigned bytes from NCL.
C  (2) The input string of bytes is returned by NCL's fbinrecread function.
C  (3) Uses byte routines from John Burkardt (included here)
C 
C !REVISION HISTORY: 
C  04 Dec 2013 - R. Yantosca - Initial version
CEOP
C------------------------------------------------------------------------------
CBOC
C
C !LOCAL VARIABLES:
C
      INTEGER     I, J, N, X
      CHARACTER*1 C(13248)

C     !----------------------------------------------------------------
C     ! Split characters into an array for use 
C     ! w/ the byte conversion routines below
C     !----------------------------------------------------------------

      DO I = 1, 13248
         C(I) = BYTES(I:I)
      ENDDO

C     !----------------------------------------------------------------
C     ! Parse into REAL*4
C     ! (Bytes are returned from NCL's fbinrecread function)
C     !----------------------------------------------------------------
      N = 0

      DO I = 1, NI
      DO J = 1, NJ

         ! First convert byte to unsigned integer
         CALL B4_TO_UI4    ( C(N:N+3), X            )

         ! Then convert unsigned integer to REAL*4
         CALL B4_IEEE_TO_R4( X,        ARRAY(NI,NJ) )

         ! Skip counter ahead by 4 bytes
         N = N + 4
      ENDDO
      ENDDO
      
      RETURN
      END SUBROUTINE MET_2D
CEOC
C------------------------------------------------------------------------------
C          Harvard University Atmospheric Chemistry Modeling Group            !
C------------------------------------------------------------------------------
CBOP
C
C !IROUTINE: b4_to_ui4
C
C !DESCRIPTION: B4\_TO\_UI4 converts four bytes to an unsigned integer. 
C\\
C\\
C !INTERFACE:
C
      SUBROUTINE B4_TO_UI4( b4, i4 )
C
C !INPUT PARAMETERS: 
C
      CHARACTER*1 b4(4)  ! Bytes to be converted
C
C !OUTPUT PARAMETERS:
C
      INTEGER     i4     ! Unsigned INTEGER*4
C
C !AUTHOR:
C  John Burkardt
C
C !REVISION HISTORY: 
C  01 Oct 1995 - R. Yantosca - Initial version
C  04 Dec 2013 - R. Yantosca - Added ProTeX headers
CEOP
C------------------------------------------------------------------------------
CBOC
C
C !LOCAL VARIABLES:
C
      INTEGER i

C     !-----------------------------------------------------------------
C     ! Convert bytes to unsigned integer
C     !-----------------------------------------------------------------

C     ! Initialize
      i4 = 0

C     ! Add value from each byte
      DO i = 1, 4         
         i4 = i4 * 256 + ICHAR( b4(i) )
      ENDDO

      RETURN
      END SUBROUTINE B4_TO_UI4
CEOC
C------------------------------------------------------------------------------
C          Harvard University Atmospheric Chemistry Modeling Group            !
C------------------------------------------------------------------------------
CBOP
C
C !IROUTINE: b4_ieee_to_r4
C
C !DESCRIPTION: Routine B4\_IEEE\_TO\_R4 converts a 4 byte IEEE word into a 
C  real value.
C\\
C\\
C !INTERFACE:
C
      SUBROUTINE B4_IEEE_TO_R4( word, r )
C
C !INPUT PARAMETERS: 
C
      INTEGER word   ! 4-byte IEEE word (output from B4_TO_UI4)
C
C !OUTPUT PARAMETERS:
C
      REAL*4  r      ! REAL*4 value corresponding to WORD
C
C !AUTHOR:
C  John Burkardt
C
C !REMARKS:
C  Discussion:
C  -----------
C  This routine does not seem to working reliably for unnormalized data.
C                                                                             .
C  The word containing the real value may be interpreted as:
C                                                                             .
C    /SEEEEEEE/EFFFFFFF/FFFFFFFF/FFFFFFFF/
C                                                                             .
C    /33222222/22222222/22222100/00000000/
C    /10987654/32109876/54321098/76543210/  <-- Bit numbering
C                                                                             .
C  where
C                                                                             .
C    S is the sign bit,
C    E are the exponent bits,
C    F are the mantissa bits.
C                                                                             .
C  The mantissa is usually "normalized"; that is, there is an implicit
C  leading 1 which is not stored.  However, if the exponent is set to
C  its minimum value, this is no longer true.
C                                                                             .
C  The exponent is "biased".  That is, you must subtract a bias value
C  from the exponent to get the true value.
C                                                                             .
C  If we read the three fields as integers S, E and F, then the
C  value of the resulting real number R can be determined by:
C                                                                             .
C  * if E = 255
C      if F is nonzero, then R = NaN;
C      if F is zero and S is 1, R = -Inf;
C      if F is zero and S is 0, R = +Inf;
C  * else if E > 0 then R = (-1)**(S) * 2**(E-127) * ( 1 + ( F/(2**24) )
C  * else if E = 0
C      if F is nonzero, R = (-1)**(S) * 2**(E-126) * ( F/(2**24)       )
C      if F is zero and S is 1, R = -0;
C      if F is zero and S is 0, R = +0;! 
C                                                                             .
C  With a little algebra, we can rewrite the expressions:
C                                                                             .
C      R = (-1)**(S) * 2**(E-127) * ( 1 + (F/2**24) ) 
C      R = (-1)**(S) * 2**(E-126) * ( F/(2**24)     )
C                                                                             .
C  as:
C                                                                             .
C      R = (-1)**(S) * 2**(E-127-23) * ( 8388608 + F ) 
C      R = (-1)**(S) * 2**( -126-23) * ( F           )
C                                                                             .
C  which are more computationally efficient.  Here 8388608 = 2**23.
C                                                                             .
C  Reference:
C  ----------
C  ANSI/IEEE Standard 754-1985,
C  Standard for Binary Floating Point Arithmetic.
C
C !REVISION HISTORY: 
C  10 Nov 2001 - J. Burkardt - Initial version
C  04 Dec 2013 - R. Yantosca - Added ProTeX headers
CEOP
C------------------------------------------------------------------------------
CBOC
C
C !LOCAL VARIABLES:
C
      INTEGER s, e, f

C     !-----------------------------------------------------------------
C     ! Read the sign, exponent, and mantissa fields
C     !-----------------------------------------------------------------

C     ! Extract SIGN, located at bit #31 of WORD
      s = 0
      CALL MVBITS( word, 31, 1, s, 0 )

C     ! Extract EXPONENT, located at bits #23-30 of WORD
      e = 0
      CALL MVBITS( word, 23, 8, e, 0 )

C     ! Extract MANTISSA (aka FRACTIONAL PART),
C     ! located at bits #0-22 of WORD
      f = 0
      CALL MVBITS( word, 0, 23, f, 0 )

C     !-----------------------------------------------------------------
C     ! Construct the real value from S, E, F fields.
C     ! Don't bother trying to return NaN or Inf just yet.
C     !-----------------------------------------------------------------
      IF ( e == 255 ) THEN

         r = 0.0E+00

      ELSE IF ( e > 0 ) THEN

         r = ( ( -1.0E+00 )**s       )
     &     * ( 2e0**( e - 127 - 23 ) )
     &     * ( REAL( 8388608 + f )   )

      ELSE IF ( e == 0 ) THEN

         r = ( ( -1.0E+00 )**s       )
     &     * ( 2e0**( -126 - 23 )    )
     &     * ( REAL( f )             )

      ENDIF

      RETURN
      END SUBROUTINE B4_IEEE_TO_R4
CEOC
C------------------------------------------------------------------------------
C          Harvard University Atmospheric Chemistry Modeling Group            !
C------------------------------------------------------------------------------
CBOP
C
C !IROUTINE: b8_to_ui8
C
C !DESCRIPTION: B4\_TO\_UI8 converts eight bytes to an unsigned integer. 
C\\
C\\
C !INTERFACE:
C
      SUBROUTINE B8_TO_UI8( b8, i8 )
C
C !INPUT PARAMETERS: 
C
      CHARACTER*1 b8(8)  ! Bytes to be converted
C
C !OUTPUT PARAMETERS:
C
      INTEGER*8   i8     ! Unsigned INTEGER*8
C
C !AUTHOR:
C  John Burkardt
C 
C !REVISION HISTORY: 
C  07 Nov 2001 - J. Burkardt - Initial version
C  04 Dec 2013 - R. Yantosca - Converted to F77 format, this is easier to
C                              interface w/ NCL via WRAPIT
C  04 Dec 2013 - R. Yantosca - Added ProTeX headers
CEOP
C------------------------------------------------------------------------------
CBOC
C
C !LOCAL VARIABLES:
C
      INTEGER i

C     !-----------------------------------------------------------------
C     ! Convert bytes to unsigned integer
C     !-----------------------------------------------------------------
      i8 = 0
      
      DO i = 1, 8
         i8 = i8 * 256 + ICHAR( b8(i) )
      ENDDO

      RETURN
      END SUBROUTINE B8_TO_UI8
CEOC
C------------------------------------------------------------------------------
C          Harvard University Atmospheric Chemistry Modeling Group            !
C------------------------------------------------------------------------------
CBOP
C
C !IROUTINE: b8_ieee_to_r8
C
C !DESCRIPTION: Routine B8\_IEEE\_TO\_R8 converts an 8 byte IEEE word 
C  into a real value.
C\\
C\\
C !INTERFACE:
C
      SUBROUTINE B8_IEEE_TO_R8( word, r )
C
C !INPUT PARAMETERS: 
C
      INTEGER*8 word   ! 8-byte word (output from B8_TO_UI8)
C
C !OUTPUT PARAMETERS:
C
      REAL*8    r      ! REAL*8 value corresponding to WORD
C
C !AUTHOR:
C  John Burkardt
C
C !REMARKS:
C  Discussion:
C  -----------
C  This routine does not seem to working reliably for unnormalized data.
C                                                                             .
C  The word containing the real value may be interpreted as:
C                                                                             .
C  /SEEEEEEE/EEEEFFFF/FFFFFFFF/FFFFFFFF/FFFFFFFF/FFFFFFFF/FFFFFFFF/FFFFFFFF/
C                                                                             .
C  /66665555/55555544/44444444/33333333/33222222/22221111/11111100/00000000/
C  /32109876/54321098/76543210/98765432/10987654/32109876/54321098/76543210/
C      -- Bit numbering   
C                                                                             .
C  where
C                                                                             .
C    S is the sign bit,
C    E are the exponent bits,
C    F are the mantissa bits.
C                                                                             .
C  The mantissa is usually "normalized"; that is, there is an implicit
C  leading 1 which is not stored.  However, if the exponent is set to
C  its minimum value, this is no longer true.
C                                                                             .
C  The exponent is "biased".  That is, you must subtract a bias value
C  from the exponent to get the true value.
C                                                                             .
C  If we read the three fields as integers S, E and F, then the
C  value of the resulting real number R can be determined by:
C                                                                             .
C  * if E = 2047
C      if F is nonzero, then R = NaN;
C      if F is zero and S is 1, R = -Inf;
C      if F is zero and S is 0, R = +Inf;
C  * else if E > 0 then R = (-1)**(S) * 2**(E-1023) * ( 1 + ( F/(2**53) )
C  * else if E = 0
C      if F is nonzero, R = (-1)**(S) * 2**(E-1022) * ( F/(2**53)       )
C      if F is zero and S is 1, R = -0;
C      if F is zero and S is 0, R = +0; 
C                                                                             .
C  With a little algebra, we can rewrite the expressions:
C                                                                             .
C      R = (-1)**(S) * 2**(E-1023-52) * ( 1 + F/(2**53) ) 
C      R = (-1)**(S) * 2**( -1022-52) * ( F/(2**53)     )
C                                                                             .
C  as:
C                                                                             .
C      R = (-1)**(S) * 2**(E-1023-52) * ( 4.503599627370496d+015 + F ) 
C      R = (-1)**(S) * 2**( -1022-52) * ( F                          )
C                                                                             .
C  which are more computationally efficient.  
C  Here, the term 4.503599627370496d+015 = 2**52.
C                                                                             .
C  Reference:
C  ----------
C  ANSI/IEEE Standard 754-1985,
C  Standard for Binary Floating Point Arithmetic.
C
C !REVISION HISTORY: 
C  10 Nov 2001 - J. Burkardt - Initial version
C  04 Dec 2013 - R. Yantosca - Converted to F77 format, this is easier to
C                              interface w/ NCL via WRAPIT
C  04 Dec 2013 - R. Yantosca - Added ProTeX headers
CEOP
C------------------------------------------------------------------------------
CBOC
C
C !LOCAL VARIABLES:
C
      INTEGER*8 s, e, f

C     !-----------------------------------------------------------------
C     ! Read the sign, exponent, and mantissa fields
C     !-----------------------------------------------------------------

C     ! Extract SIGN, located at bit #63 of WORD
      s = 0
      CALL MVBITS( word, 63,  1, s, 0 )

C     ! Extract EXPONENT, located at bits #52-62 of WORD
      e = 0
      CALL MVBITS( word, 52, 11, e, 0 )
      
C     ! Extract MANTISSA (aka FRACTIONAL PART),
C     ! located at bits #0-51 of WORD
      f = 0
      CALL MVBITS( word,  0, 52, f, 0 )

C     !-----------------------------------------------------------------
C     ! Construct the real value from S, E, F fields.
C     ! Don't bother trying to return NaN or Inf just yet.
C     !-----------------------------------------------------------------
      IF ( e == 2047 ) THEN

         r = 0d0

      ELSE IF ( e > 0 .and. e < 2047 ) THEN

         r = ( ( -1d0 )**s                        )
     &     * ( 2d0**( e-1023-52 )                 )
     &     * ( 4.503599627370496d+015 + DBLE( f ) )

      ELSE IF ( e == 0 ) THEN

         r = ( ( -1d0 )**s                        )
     &     * ( 2d0**( -1022-52 )                  )
     &     * ( DBLE( f )                          )

      ENDIF

      RETURN
      END SUBROUTINE B8_IEEE_TO_R8
!EOC
