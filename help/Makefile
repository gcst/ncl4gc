#------------------------------------------------------------------------------
#    NCL4GC: NCL Tools for use with the GEOS-Chem Chemical Transport Model    !
#------------------------------------------------------------------------------
#BOP
#
# !MODULE: Makefile (in NGL4GC/help subdirectory)
#
# !DESCRIPTION: Displays the makefile help screen for NCL4GC.
#\\
#\\
# !REMARKS:
# To build the programs, call "make" with the following syntax:
#                                                                             .
#   make -TARGET [ OPTIONAL-FLAGS ]
#                                                                             .
# To display a complete list of options, type "make help".
#
# !REVISION HISTORY: 
#  06 Dec 2013 - R. Yantosca - Initial version
#EOP
#------------------------------------------------------------------------------
#BOC

# Get the Unix shell (in SHELL variable) from Makefile_header.mk
SHELL :=/bin/bash

.PHONY: all help

all: help

help:
	@echo '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
	@echo '%%%              NCL4GC HELP SCREEN               %%%'
	@echo '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
	@echo
	@echo 'Usage: make TARGET [ OPTIONAL-FLAGS ]'
	@echo ''
	@echo ''
	@echo 'TARGET may be one of the following:'
	@echo 'all             Default target (synonym for "install doc")'
	@echo 'lib             Builds shared object libraries for NCL4GC in fortran/'
	@echo 'doc             Builds NCL4GC documentation (*.ps, *.pdf) in doc/'
	@echo 'help            Displays this help screen'
	@echo 'docclean        Removes shared object libraries'
	@echo 'libclean        Removes shared object libraries'
	@echo 'realclean       Synonym for docclean libclean'

	@echo ''
	@echo  ''	       
	@echo 'OPTIONAL-FLAGS may be:'
	@echo 'COMPILER=___    Specifies the Fortran compiler for building shared libraries'
	@echo '                --> Options: ifort pgi lahey g77 g95 gfortran'
	@echo 'DEBUG=yes       Builds shared object libraries with debugging options'
