;------------------------------------------------------------------------------
;    NCL4GC: NCL Tools for use with the GEOS-Chem Chemical Transport Model    !
;------------------------------------------------------------------------------
;BOP
;
; !ROUTINE: hco_adj_edgar_nox
;
; !DESCRIPTION: Routine to adjust the EDGAR v4.2 input files for better
;  COARDS compliance.
;\\
;\\
; !USES:
;
 ; NCL routines
 load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"  

 ; Local routines
 load "$NCL4GC/file_io/add_coards_var_atts.ncl"
 load "$NCL4GC/file_io/add_coards_global_atts.ncl"
;
; !REMARKS:
;  (1) Adds a time dimension to emission variable
;  (2) Reorder longitude to be centered around 0 degrees using lonFlip function
;
; !EXAMPLE:
; ncl hco_adj_edgar_nox.ncl 'inFile="file1"' 'outFile="file2"'
;
; !REVISION HISTORY:
;  23 Jan 2015 - M. Sulprizio- Initial version
;EOP
;------------------------------------------------------------------------------
;BOC
begin

  ;=========================================================================
  ; Error check inputs
  ;=========================================================================
  
  ; Make sure inFile is passed
  if ( .not. isvar( "inFile" ) ) then 
    print( "Argument 'inFile' not specified...exiting!" )
  end if

  ; Make sure inFile is passed
  if ( .not. isvar( "outFile" ) ) then 
    print( "Argument 'outFile' not specified...exiting!" )
  end if

  ; Make sure inFile is passed
  if ( .not. isvar( "year" ) ) then 
    print( "Argument 'year' not specified...exiting!" )
  end if

  ;=========================================================================
  ; Calculate time variables
  ;=========================================================================

  ; Convert year string to integer
  inYear                    = stringtointeger( year )
  Date                      = inYear*10000+101

  ; Tau0 is an absolute Julian time in hours since the start of GEOS-1 epoch
  base_julian               = greg2jul(1985,1,1,0)
  this_julian               = greg2jul(inYear,1,1,0) - base_julian
  Tau0                      = this_julian * 24

  ;=========================================================================
  ; Read data from the input file
  ;=========================================================================
  fIn                       = addfile( inFile, "r" )
  lat                       = fIn->lat
  lon                       = fIn->lon
  emi_nox                   = fIn->emi_nox

  ; Dimensions
  nLat                      = dimsizes( lat )
  nLon                      = dimsizes( lon )

  Delta_Lon                 = decimalPlaces((lon(1)-lon(0)),2,True)
  Delta_Lat                 = decimalPlaces((lat(1)-lat(0)),2,True)

  ;=========================================================================
  ; Add modifications for COARDS compliance
  ;=========================================================================

  ;---------------
  ; TIME 
  ;---------------

  time                      = new( (/ 1 /), double )
  time!0                    = "time"
  time@calendar             = "gregorian"
  time@long_name            = "time"
  time@units                = "hours since 1985-01-01 00:00:00"
  time@_FillValue           = 1d15
  time&time                 = time
  time(0)                   = Tau0

  ;---------------
  ; EMISSIONS
  ;---------------
  NO                        = new( (/ 1, nLat, nLon /), float )
  NO!0                      = "time"
  NO!1                      = "lat"
  NO!2                      = "lon"
  NO&time                   = time
  NO&lat                    = lat
  NO&lon                    = lon
  NO@long_name              = "NO"
  NO@units                  = "kg/m2/s"
  NO@missing_value          = 1e+15
  NO@_FillValue             = 1e+15
  NO@scale_factor           = 1.0
  NO@add_offset             = 0.0
  NO(:,:,:)                 = 0.0

  ; Loop over grid boxes
  do J =  0, nLat-1
    do I = 0, nLon-1
      ; Save emissions into 3D array
      NO(0,J,I) = (/ emi_nox(J,I) /)
    end do
  end do

  ; Convert from kg(N)/m2/s to kg(NO)/m2/s by multiplying by 30/14
  NO = NO * ( 30.0 / 14.0)

  ; Debug
  printMinMax( NO, True )

  ;---------------
  ; LON
  ;---------------

  ; Reorder array about the central longitude coordinate variable
  NO = lonFlip( NO )

  ;=========================================================================
  ; Save to output file
  ;=========================================================================

  ; Print data (uncomment for debugging)
  ;printVarSummary( time )
  ;printVarSummary( lon  )
  ;printVarSummary( lat  )
  printVarSummary( NO   ) 

  ; Open output file (remove prior version)
  system( "rm -f " + outFile )
  fOut                      = addfile( outFile, "c" )
  ; Make time unlimited
  filedimdef(fOut,"time",-1,True)
  fOut->time                = time
  fOut->lat                 = lat
  fOut->lon                 = lon
  fOut->NO                  = NO

  ; Add global file attributes
  ; Redefine the title string
  fOut@Title                = "EDGAR v4.2 NO emissions for GEOS-Chem"
  fOut@Conventions          = "COARDS" 
  fOut@Model                = "GENERIC"
  fOut@Source               = fIn@source
  fOut@References           = fIn@references
  fOut@History              = "Last modified on: " + systemfunc( "date" )
  fOut@ProductionDateTime   = "Last modified on: " + systemfunc( "date" )
  fOut@ModificationDateTime = "Last modified on: " + systemfunc( "date" )
  fOut@Nlayers              = 1
  fOut@Start_Date           = Date
  fOut@Start_Time           = 0
  fOut@End_Date             = Date
  fOut@End_Time             = 0
  fOut@Delta_Time           = 0
  fOut@Delta_Lon            = Delta_Lon
  fOut@Delta_Lat            = Delta_Lat

end
;EOC