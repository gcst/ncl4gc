;------------------------------------------------------------------------------
;    NCL4GC: NCL Tools for use with the GEOS-Chem Chemical Transport Model    !
;------------------------------------------------------------------------------
;BOP
;
; !ROUTINE: hco_adj_global_thno3
;
; !DESCRIPTION: Routine to adjust the monthly mean THNO3 (= HNO3 + NIT) input
; files for use in HEMCO with the offline aerosol simulation.
;\\
;\\
; !USES:
;
 ; NCL routines
 load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"  

 ; Local routines
 load "$NCL4GC/file_io/add_coards_var_atts.ncl"
 load "$NCL4GC/file_io/add_coards_global_atts.ncl"
;
; !REMARKS:
; (1) Make sure longitudes are all monotonically increasing
;
; !REVISION HISTORY:
;  18 Sep 2014 - M. Sulprizio- Initial version
;EOP
;------------------------------------------------------------------------------
;BOC
begin

;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
; For testing
  inDir         = "/home/mpayer/T/v10-01e/AEROSOL/sulfate_sim_200508/nc/"
  inFile        = inDir+"THNO3.geos5.4x5.nc"

  outDir        = "/home/mpayer/GC/data/ExtData/HEMCO/OFFLINE_AEROSOL/"
  outFile       = outDir+"v2014-09/THNO3.geos5.4x5.nc"
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ;=========================================================================
  ; Error check inputs
  ;=========================================================================
  
  ; Make sure inFile is passed
  if ( .not. isvar( "inFile" ) ) then 
    print( "Argument 'inFile' not specified...exiting!" )
  end if

  ; Make sure inFile is passed
  if ( .not. isvar( "outFile" ) ) then 
    print( "Argument 'outFile' not specified...exiting!" )
  end if

  ;=========================================================================
  ; Read data from the input file
  ;=========================================================================

  fIn                       = addfile( inFile, "r" )
  lon                       = fIn->lon
  lat                       = fIn->lat
  lev                       = fIn->lev
  time                      = fIn->time
  THNO3                     = fIn->IJ_AVG_S__HNO3
 
  ; Dimensions
  nLon                      = dimsizes( lon  )
  nLat                      = dimsizes( lat  )
  nLev                      = dimsizes( lev  )
  nTime                     = dimsizes( time )

  ; Debug
  print("nLon  "+nLon)
  print("nLat  "+nLat)
  print("nLev  "+nLev)
  print("nTime "+nTime)

  ;=========================================================================
  ; Add modifications for COARDS compliance
  ;=========================================================================

  ;---------------
  ; LON
  ;---------------

  ; Make sure longitudes are all monotonically increasing
  lon                       = where( lon .ge. 180.0, lon - 360.0, lon )
  lon&lon                   = lon
  THNO3&lon                 = lon

  ;---------------
  ; Var atts
  ;---------------

  ; Also add the gamap_category attribute
  THNO3@gamap_category      = "IJ_AVG_S__HNO3"
  THNO3@long_name           = "Total gas + aerosol nitrate"

  ; Add common COARDS variable attributesq
  add_coards_var_atts( THNO3 ) 

  ;=========================================================================
  ; Save to output file
  ;=========================================================================

  ; Print data (uncomment for debugging)
  printVarSummary( lon   )
  printVarSummary( lat   )
  printVarSummary( lev   )
  printVarSummary( time  )
  printVarSummary( THNO3 ) 

  ; Open output file (remove prior version)
  system( "rm -f " + outFile )
  fOut                      = addfile( outFile, "c" )
  fOut->lon                 = lon
  fOut->lat                 = lat
  fOut->lev                 = lev

  ; Make time unlimited
  filedimdef(fOut,"time",-1,True)
  fOut->time                = time

  ; Write variable to file
  fOut->THNO3               = THNO3

  ; Add global file attributes
  ; Redefine the title string
  fOut@Title                = "Global monthly mean HNO3+NIT values from a 1-yr simulation with GEOS-Chem v7-02-03"
  fOut@Conventions          = "COARDS" 
  fOut@History              = "Last modified on: " + systemfunc( "date" )
  fOut@ProductionDateTime   = "Last modified on: " + systemfunc( "date" )
  fOut@ModificationDateTime = "Last modified on: " + systemfunc( "date" )
  fOut@Nlayers              = 1
  fOut@Start_Date           = fIn@Start_Date
  fOut@Start_Time           = fIn@Start_Time
  fOut@End_Date             = fIn@End_Date
  fOut@End_Time             = fIn@End_Time
  fOut@Delta_Time           = fIn@Delta_Time
  fOut@Delta_Lon            = fIn@Delta_Lon
  fOut@Delta_Lat            = fIn@Delta_Lat

end
;EOC