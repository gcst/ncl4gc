;------------------------------------------------------------------------------
;    NCL4GC: NCL Tools for use with the GEOS-Chem Chemical Transport Model    !
;------------------------------------------------------------------------------
;BOP
;
; !ROUTINE: hco_adj_global_ph2o2
;
; !DESCRIPTION: Routine to adjust the monthly mean PH2O2 input files for use in
;  HEMCO with the offline aerosol simulation.
;\\
;\\
; !USES:
;
 ; NCL routines
 load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"  

 ; Local routines
 load "$NCL4GC/file_io/add_coards_var_atts.ncl"
 load "$NCL4GC/file_io/add_coards_global_atts.ncl"
;
; !REMARKS:
; (1) Make sure longitudes are all monotonically increasing
; (2) Make sure data has correct number of levels for HEMCO
; (3) Convert from molec/cm3/s to kg/m3/s for HEMCO
;
; !REVISION HISTORY:
;  18 Sep 2014 - M. Sulprizio- Initial version
;  19 Sep 2014 - M. Sulprizio- Expand data to 30 levels (GEOS-4) or 47 levels
;                              (GEOS-5/GEOS-FP/MERRA)
;EOP
;------------------------------------------------------------------------------
;BOC
begin

;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
; For testing
  inDir         = "/home/mpayer/T/v10-01e/AEROSOL/sulfate_sim_200508/nc/"
  inFile        = inDir+"PH2O2.geos5.4x5.nc"

  outDir        = "/home/mpayer/GC/data/ExtData/HEMCO/OFFLINE_AEROSOL/"
  outFile       = outDir+"v2014-09/PH2O2.geos5.4x5.nc"
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ;=========================================================================
  ; Error check inputs
  ;=========================================================================
  
  ; Make sure inFile is passed
  if ( .not. isvar( "inFile" ) ) then 
    print( "Argument 'inFile' not specified...exiting!" )
  end if

  ; Make sure inFile is passed
  if ( .not. isvar( "outFile" ) ) then 
    print( "Argument 'outFile' not specified...exiting!" )
  end if

  ;=========================================================================
  ; Read data from the input file
  ;=========================================================================

  fIn                       = addfile( inFile, "r" )
  lon                       = fIn->lon
  lat                       = fIn->lat
  time                      = fIn->time
  inLev                     = fIn->lev
  inPH2O2                   = fIn->PORL_L_S__PH2O2
 
  ; Dimensions
  nLon                      = dimsizes( lon  )
  nLat                      = dimsizes( lat  )
  nLev                      = dimsizes( inLev)
  nTime                     = dimsizes( time )

  ; Debug
  print("nLon       "+nLon  )
  print("nLat       "+nLat  )
  print("nTime      "+nTime )
  print("nLev (in)  "+nLev  )

  ;=========================================================================
  ; Add modifications for COARDS compliance
  ;=========================================================================

  ; Molecules of H2O2 per kg of H2O2 [molec/kg]
  XNUMOL_H2O2 = 6.022e23 / 34e-3

  ;---------------
  ; LEV
  ;---------------

  if ( nLev .eq. 38 ) then

    ;--------------------------
    ; GEOS-5 / GEOS-FP / MERRA
    ;--------------------------

    ; Expand levels from 38 to 47
    dumLev   = (/ 0.033814, 0.023755, 0.014342, 0.006588, 0.002816, \
                  0.001109, 0.000399, 0.000127, 0.000028  /)
    nLev_out = nLev + dimsizes( dumLev )
    lev      = array_append_record ( inLev, dumLev, 0 )
    print("nLev (out) "+nLev_out)

    ; Add extra levels to data and set values to zero
    PH2O2    = new((/nTime,nLev_out,nLat,nLon/), float)
    PH2O2    = 0e0

    ; Fill array with data from input file
    do T = 0, nTime-1
    do L = 0, nLev-1
    do J = 0, nLat-1
    do I = 0, nLon-1
    
      PH2O2(T,L,J,I) = inPH2O2(T,L,J,I)

      ; Convert from molec/cm3/s to kg/m3/s
      PH2O2(T,L,J,I) = PH2O2(T,L,J,I) / XNUMOL_H2O2 * 1e6
    
    end do
    end do
    end do
    end do

  else if ( nLev .eq. 17 ) then

    ;--------------------------
    ; GEOS-4
    ;--------------------------

    ; Expand levels from 17 to 30
    dumLev   = (/ 0.099191, 0.084313, 0.066559, 0.047641,           \
                  0.033814, 0.023755, 0.014342, 0.006588, 0.002816, \
                  0.001109, 0.000399, 0.000127, 0.000028            /)
    nLev_out = nLev + dimsizes( dumLev )
    lev      = array_append_record ( inLev, dumLev, 0 )
    print("nLev (out) "+nLev_out)

    ; Add extra levels to data and set values to zero
    PH2O2    = new((/nTime,nLev_out,nLat,nLon/), float)
    PH2O2    = 0e0

    ; Fill array with data from input file
    do T = 0, nTime-1
    do L = 0, nLev-1
    do J = 0, nLat-1
    do I = 0, nLon-1
    
      PH2O2(T,L,J,I) = inPH2O2(T,L,J,I)

      ; Convert from molec/cm3/s to kg/m3/s
      PH2O2(T,L,J,I) = PH2O2(T,L,J,I) / XNUMOL_H2O2 * 1e6
    
    end do
    end do
    end do
    end do

  else

    ; Use dimensions of input data
    lev   = inLev
    PH2O2 = inPH2O2

    do T = 0, nTime-1
    do L = 0, nLev-1
    do J = 0, nLat-1
    do I = 0, nLon-1

      ; Convert from molec/cm3/s to kg/m3/s
      PH2O2(T,L,J,I) = PH2O2(T,L,J,I) / XNUMOL_H2O2 * 1e6

    end do
    end do
    end do
    end do

  end if
  end if

  ; Update coordinate variable
  lev&lev   = lev
  PH2O2&lev = lev

  ;---------------
  ; LON
  ;---------------

  ; Make sure longitudes are all monotonically increasing
  lon                       = where( lon .ge. 180.0, lon - 360.0, lon )
  lon&lon                   = lon
  PH2O2&lon                 = lon

  ;---------------
  ; Var atts
  ;---------------

  ; Also add the gamap_category attribute
  PH2O2@gamap_category      = "JV_MAP_S__PH2O2"
  PH2O2@long_name           = "P(H2O2) values"
  PH2O2@units               = "kg/m3/s"

  ; Add common COARDS variable attributesq
  add_coards_var_atts( PH2O2 ) 

  ;=========================================================================
  ; Save to output file
  ;=========================================================================

  ; Print data (uncomment for debugging)
  printVarSummary( lon   )
  printVarSummary( lat   )
  printVarSummary( lev   )
  printVarSummary( time  )
  printVarSummary( PH2O2 ) 

  ; Open output file (remove prior version)
  system( "rm -f " + outFile )
  fOut                      = addfile( outFile, "c" )
  fOut->lon                 = lon
  fOut->lat                 = lat
  fOut->lev                 = lev

  ; Make time unlimited
  filedimdef(fOut,"time",-1,True)
  fOut->time                = time

  ; Write variable to file
  fOut->PH2O2               = PH2O2

  ; Add global file attributes
  ; Redefine the title string
  fOut@Title                = "Global monthly mean P(H2O2) values from a 1-yr simulation with GEOS-Chem v7-02-03"
  fOut@Conventions          = "COARDS" 
  fOut@History              = "Last modified on: " + systemfunc( "date" )
  fOut@ProductionDateTime   = "Last modified on: " + systemfunc( "date" )
  fOut@ModificationDateTime = "Last modified on: " + systemfunc( "date" )
  fOut@Nlayers              = 1
  fOut@Model                = fIn@Model
  fOut@Start_Date           = fIn@Start_Date
  fOut@Start_Time           = fIn@Start_Time
  fOut@End_Date             = fIn@End_Date
  fOut@End_Time             = fIn@End_Time
  fOut@Delta_Time           = fIn@Delta_Time
  fOut@Delta_Lon            = fIn@Delta_Lon
  fOut@Delta_Lat            = fIn@Delta_Lat

end
;EOC