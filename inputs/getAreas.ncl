;------------------------------------------------------------------------------
;    NCL4GC: NCL Tools for use with the GEOS-Chem Chemical Transport Model    !
;------------------------------------------------------------------------------
;BOP
;
; !ROUTINE: getAreas
;
; !DESCRIPTION: Returns the surface areas for a given grid.
;\\
;\\
; !INTERFACE:
;
 undef( "getAreas" )
 procedure getAreas( grid:string, area:float )
;
; !INPUT PARAMETERS:
;  grid: Name of the input grid (e.g. "gmao2x25", "gmao4x5", etc.)
;
; !OUTPUT PARAMETERS:
;  area: Array containing the surface area of each grid box [m2].
;
; !LOCAL VARIABLES:
;
 local Deg_to_Rad, Re_2,   rootDir,  inFileName, in,        \
       dum,        nLat,   nLon,     latEdgRad,  lonEdgRad, \
       sine_N,     sine_S, sine_diff
;
; !CALLING SEQUENCE:
;  ncl 'grid="gmao2x25"' getAreas.ncl   etc
;
; !REMARKS:
;  Assumes a Cartesian grid, which is fine for most emissions etc. data.
;
; !REVISION HISTORY:
;  13 Mar 2014 - R. Yantosca - Initial version
;EOP
;------------------------------------------------------------------------------
;BOC
begin

  ; Make sure the input grid is specified
  if ( .not. isvar( "grid" ) ) then
    print( "getAreas: Need to pass an input grid!" )
    exit
  end if
 
  ; Define some physical constants
  Deg_to_Rad    = 3.14159265358979323e0 / 180e0  ; Degrees to radians
  Re_2          = 6.375e+6 * 6.375e+6            ; Radius of earth, squared

  ; Get the value of the $NCL4GC environment variable
  rootDir       = getenv( "NCL4GC" )

  ; Read the file w/ the lon & lat centers for this grid 
  inFileName    = rootDir + "/inputs/latLonEdges_" + grid + ".nc"
  in            = addfile( inFileName, "r" )
  dum           = in->dum

  ; Get the # of lons & lats
  nLat          = dimsizes( dum&lat ) - 1
  nLon          = dimsizes( dum&lon ) - 1

  ; Latitude and longitude edges [radians]
  latEdgRad     = dum&lat * Deg_to_Rad
  lonEdgRad     = dum&lon * Deg_to_Rad

  ; Delta longitude [radians]
  dLonRad       = lonEdgRad(1) - lonEdgRad(0)

  ; Define area array
  area          = new( (/ nLat, nLon /), float )

  ; Compute surface areas
  do J = 0, nLat-1

    ; Sine of latitude at north and south edges of grid box
    sine_N      = sin( latEdgRad(J+1) )
    sine_S      = sin( latEdgRad(J  ) )  

    ; Difference in 
    sine_diff   = sine_N - sine_S

    ; 
    area(J,:)   = dLonRad * Re_2 * sine_diff     
  end do

  print( "sum area [m2]: " + sum( area ) )

end